#include "../SiElementPropertiesTableCondAlg.h"
#include "SiSpacePointFormation/SiTrackerSpacePointFinder.h"
#include "SiSpacePointFormation/TrkToXAODSpacePointConversion.h"
 
DECLARE_COMPONENT( InDet::SiElementPropertiesTableCondAlg )
DECLARE_COMPONENT( InDet::SiTrackerSpacePointFinder )
DECLARE_COMPONENT( InDet::TrkToXAODSpacePointConversion )
